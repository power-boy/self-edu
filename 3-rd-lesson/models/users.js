var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var usersSchema = new Schema({
    name: 'String',
    login: 'String',
    email: 'String',
    telephone: 'String'
});

module.exports = mongoose.model('Users', usersSchema);