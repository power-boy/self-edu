var gulp = require('gulp')
    , uglify = require('gulp-uglify')
    , concat = require('gulp-concat')
    , ngAnnotate = require('gulp-ng-annotate');

gulp.task('minify', function() {
    gulp.src(['./public/js/*.js'])
        .pipe(concat('index.js'))
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(gulp.dest('./public/dist'));
});

gulp.task('build', function(){
    gulp.run('minify');

    gulp.watch("./public/js/*.js", function(event){
        gulp.run('minify');
    });
});
