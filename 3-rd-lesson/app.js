var express = require('express');
var bodyParser = require('body-parser');
var users = require('./routes/users');
var mongoose = require('mongoose');

var app = express();
var dbName = 'users';
var connectionString = 'mongodb://localhost:27017/' + dbName;

mongoose.connect(connectionString);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(express.static(__dirname + '/public'));
app.use('/api', users);

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/public/index.html');
});

app.use(function(req, res) {
    res.status(404);
    res.send({ error: 'Route with such name doesn\'t exist' });
});

app.use(function(err, req, res, next) {
    res.status(500);
    res.send({ error: 'oops! something broke'});
});

module.exports = app;
