(function () {
    var app = angular.module('modal', ['ngRoute', 'service-module', 'ui.bootstrap']);

    app.controller('ModalCtrl', function ($scope, $modal, $log, services) {
        $scope.open = function (size) {
            var modalInstance = $modal.open({
                templateUrl: 'partials/modal.html',
                controller: 'ModalInstanceCtrl',
                size: size,
                resolve: {
                    user: function () {
                        return $scope.user;
                    }
                }
            });

            modalInstance.result.then(function() {
                services.getUsers().then(function(data) {
                    services.users = data.data;
                });
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
    });

    app.controller('ModalInstanceCtrl', function ($scope, $modalInstance, user, services) {
        if (user) {
            $scope.title = 'Edit user';
        } else {
            $scope.title = 'Add user';
        }
        $scope.user = user;

        $scope.saveUser = function (user) {
            if (user._id) {
                services.updateUser(user);
            } else {
                services.insertUser(user);
            }
            $modalInstance.close(user);
        };

        $scope.deleteUser = function (userId) {
            services.deleteUser(userId);
            $modalInstance.close();
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    });

})();