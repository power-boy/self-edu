(function () {
    var app = angular.module('list', ['service-module']);
    app.controller('ListCtrl', function($scope, services) {
        services.getUsers().then(function(data) {
            $scope.users = data.data;
        });

        $scope.$watch(function(){
            return services.users;
        }, function(newVal){
            $scope.users = newVal;
        })
    });
})();