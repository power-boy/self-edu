(function () {
    var app = angular.module('crud-app', [
        'ngRoute',
        'service-module',
        'list',
        'modal',
        'ui.bootstrap'
    ]);

    app.config(function($routeProvider) {
        $routeProvider
            .when('/', {
                controller:'ListCtrl',
                templateUrl:'partials/list.html'
            })
            .otherwise({
                redirectTo:'/'
            });
    });
})();