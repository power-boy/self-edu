var gulp = require('gulp')
    , uglify = require('gulp-uglify')
    , concat = require('gulp-concat')
    , ngAnnotate = require('gulp-ng-annotate')
    , nodemon = require('gulp-nodemon');

gulp.task('minify', function() {
    gulp.src(['./public/js/*.js'])
        .pipe(concat('index.js'))
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(gulp.dest('./public/dist'));
});

gulp.task('build', function(){
    gulp.run('minify');

    gulp.watch("./public/js/*.js", function(event){
        gulp.run('minify');
    });
});

gulp.task('demon', function () {
    nodemon({
        script: './bin/www.js',
        ext: 'js',
        env: {
            'NODE_ENV': 'development'
        }
    })
        .on('start', ['build'])
        .on('change', ['build'])
        .on('restart', function () {
            console.log('restarted!');
        });
});

// Default Task
gulp.task('default', ['demon']);