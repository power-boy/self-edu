(function () {
    var app = angular.module('list', ['service-module']);
    app.controller('ListCtrl', function($scope, services) {

        services.connect().on('update', function (users) {
            console.log('update users');
            $scope.users = users;
            $scope.$digest();
        });

        services.getUsers().then(function(data) {
            $scope.users = data.data;
        });

    });
})();