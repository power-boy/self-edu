(function () {
    var app = angular.module('service-module', []);
    app.factory("services", ['$http', function($http) {
        var serviceBase = '/api',
            user = {},
            socket;
        user.getUsers = function(){
            return $http.get(serviceBase + '/users');
        };
        user.updateUser = function (user) {
            return $http.put(serviceBase + '/users/' + user._id, {
                login : user.login,
                name : user.name,
                email : user.email,
                telephone : user.telephone
            }).then(function (status) {
                return status.data;
            });
        };
        user.insertUser = function (user) {
            return $http.post(serviceBase + '/users', {
                login : user.login,
                name : user.name,
                email : user.email,
                telephone : user.telephone
            });
        };
        user.deleteUser = function (userId) {
            return $http.delete(serviceBase + '/users/' + userId).then(function (status) {
                return status.data;
            });
        };
        user.connect = function () {
            if (!socket) {
                socket = io.connect();
            }
            return socket;
        };
        return user;
    }]);
})();