(function () {
    var app = angular.module('modal', ['ngRoute', 'service-module', 'ui.bootstrap']);

    app.controller('ModalCtrl', function ($scope, $modal, $log, services) {
        $scope.open = function (size) {
            var modalInstance = $modal.open({
                templateUrl: 'partials/modal.html',
                controller: 'ModalInstanceCtrl',
                size: size,
                resolve: {
                    user: function () {
                        return $scope.user;
                    },
                    users: function () {
                        return $scope.users;
                    }
                }
            });

            modalInstance.result.then(function() {

            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
    });

    app.controller('ModalInstanceCtrl', function ($scope, $modalInstance, user, services, users) {
        if (user) {
            $scope.title = 'Edit user';
        } else {
            $scope.title = 'Add user';
        }
        $scope.user = user;

        $scope.saveUser = function (user) {
            console.log('send data to server');
            if (user._id) {
                services.updateUser(user);
                services.connect().emit('update',  users);
            } else {
                services.insertUser(user).then(function(data) {
                    users.push(data.data);
                    services.connect().emit('update', users);
                });
            }
            $modalInstance.close();
        };

        $scope.deleteUser = function (userId) {
            console.log('send data to server');
            services.deleteUser(userId).then(function(){
                var i;
                for (i = 0; i < users.length; i++) {
                    if (users[i]['_id'] == userId) {
                        users.splice(i, 1);
                        break;
                    }
                }
                services.connect().emit('update',  users);
            });
            $modalInstance.close();
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    });

})();