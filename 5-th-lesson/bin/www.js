var app = require('../app');

app.set('port', 8000);

var server = app.listen(app.get('port'), function() {
  console.log('Express server listening on port ' + server.address().port);
});

var io = require('socket.io').listen(server);
io.on('connection', function(socket) {
    socket.on('update', function(users) {
        console.log('users were updated');
        io.emit('update', users);
    });
});