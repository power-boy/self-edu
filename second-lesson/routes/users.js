var Users = require('../models/users');
var express = require('express');

var router = express.Router();

router.route('/users')
    .get(function(req, res) {
       Users.find(function(err, user) {
           if (err) {
               res.json(err);
           } else {
               res.json(user);
           }
       });
    })
    .post(function(req, res) {
        Users.findOne({login : req.body.login}, function(err, user) {
            if(user) {
                res.statusCode = 400;
                return res.json('User with such login already exist');
            } else {
                user = new Users(req.body);
                user.save(function(err) {
                    if (err) {
                        res.statusCode = 400;
                        res.json(err);
                    } else {
                        res.setHeader('Location', '/users/' + user.id);
                        res.statusCode = 201;
                        res.json('User successfully added');
                    }
                });
            }
        });
    });

router.route('/users/:id')
    .put(function(req, res) {
        Users.findOne({_id : req.params.id}, function(err, user) {
            if(!user) {
                res.statusCode = 404;
                return res.json('User doesn\'t exist');
            }

            if (err) {
                res.statusCode = 400;
                res.json(err);
            } else {
                for (prop in req.body) {
                    user[prop] = req.body[prop];
                }
                user.save(function(err) {
                    if (err) {
                        res.json(err);
                    } else {
                        res.json('User ' + req.params.id + ' updated');
                    }
                });
            }
        });
    })
    .get(function(req, res) {
        Users.findOne({_id : req.params.id}, function(err, user) {
            if(!user) {
                res.statusCode = 404;
                return res.json('User doesn\'t exist');
            }

            if (err) {
                res.statusCode = 400;
                res.json(err);
            } else {
                res.json(user);
            }
        });
    })

    .delete(function(req,res) {
        Users.remove({_id : req.params.id}, function(err, user) {
            if(!user) {
                res.statusCode = 404;
                return res.json('User doesn\'t exist');
            }

            if (err) {
                res.statusCode = 400;
                res.json(err);
            } else {
                res.statusCode = 204;
                res.json('Successfully ' + user.id + ' deleted');
            }
        });
    });

module.exports = router;
