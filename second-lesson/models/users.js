var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var usersSchema = new Schema({
    name: 'String',
    login: 'String',
    password: 'String',
    email: 'String',
    telephone: 'Number'

});

module.exports = mongoose.model('Users', usersSchema);