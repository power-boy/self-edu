var express = require('express');
var bodyParser = require('body-parser');
var users = require('./routes/users');
var mongoose = require('mongoose');

var app = express();

var dbName = 'users';

var connectionString = 'mongodb://localhost:27017/' + dbName;

mongoose.connect(connectionString);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());

app.use('/api', users);

app.use(function(req, res) {
    res.status(404);
    res.send({ error: 'Route with such name doesn\'t exist' });
});

module.exports = app;
