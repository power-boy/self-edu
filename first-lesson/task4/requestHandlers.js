/**
 * Created by seryogin on 10.02.15.
 */
var querystring = require("querystring");

function hello(response, postData) {
    console.log("Request handler 'start' was called.");

    var body = '<html>'+
        '<head>'+
        '<meta http-equiv="Content-Type" content="text/html; '+
        'charset=UTF-8" />'+
        '</head>'+
        '<body>'+
        '<form action="/hello" method="post">'+
        '<input type="text" name="name"></textarea>'+
        '<input type="submit"/>'+
        '</form>'+
        '</body>'+
        '</html>';

    response.writeHead(200, {"Content-Type": "text/html"});

    if (!postData) {
        response.write(body);
    } else {
        body = "Hello " + querystring.parse(postData).name;
        body += "<br><a href='/'>come back</a>";
        response.write(body);
    }

    response.end();
}

exports.hello = hello;
