/**
 * Created by seryogin on 09.02.15.
 */

var server = require("./server");
var router = require("./router");
var requestHandlers = require("./requestHandlers");

var handle = {};
handle["/"] = requestHandlers.hello;
handle["/hello"] = requestHandlers.hello;

server.start(router.route, handle);
