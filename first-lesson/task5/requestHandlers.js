/**
 * Created by seryogin on 10.02.15.
 */

var fs = require('fs');

function hello(response) {
    var stream = fs.createReadStream(__dirname + '/views/index.html');
    response.writeHead(200, {"Content-Type": "text/html"});
    stream.pipe(response);
}

exports.hello = hello;
