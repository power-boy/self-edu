/**
 * Created by seryogin on 10.02.15.
 */
var url = require("url");
var fs = require('fs');

function route(handle, pathname, response) {
    console.log("About to route a request for " + pathname);
    if (typeof handle[pathname] === 'function') {
        handle[pathname](response);
    } else {
        var extension = pathname.split('.').pop(),
            stream;
        switch (extension) {
            case 'css':
                stream = fs.createReadStream(__dirname + pathname);
                response.writeHead(200, {"Content-Type": "text/css"});
                stream.pipe(response);
                break;
            case 'jpg':
                stream = fs.createReadStream(__dirname + pathname);
                response.writeHead(200, {"Content-Type": "image/jpg"});
                stream.pipe(response);
                break;
            case 'png':
                stream = fs.createReadStream(__dirname + pathname);
                response.writeHead(200, {"Content-Type": "image/png"});
                stream.pipe(response);
                break;
            default:
                response.writeHead(404, {"Content-Type": "text/plain"});
                response.write("404 Not found");
                response.end();
                break;
        }
    }
}

exports.route = route;