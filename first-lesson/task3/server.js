/**
 * Created by seryogin on 09.02.15.
 */

var http = require("http");
var url = require("url");

function start(route, handle) {
    function onRequest(request, response) {
        var pathname = url.parse(request.url).pathname;
        var content = url.parse(request.url, true).query;
        console.log("Request for " + pathname + " received.");

        route(handle, pathname, response, content);
    }

    http.createServer(onRequest).listen(8888);
    console.log("Server has started.");
}

exports.start = start;

