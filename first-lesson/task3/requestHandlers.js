/**
 * Created by seryogin on 10.02.15.
 */


function hello(response, content) {
    console.log("Request handler 'Hello' was called.");
    var headers = {"Content-Type": "text/plain", "Powered-by": "nodejs"};
    response.writeHead(200, headers);
    if (content.name) {
        response.write('hello ' + content.name);
    } else {
        response.write('hello');
    }
    response.end();
}

function bye(response, content) {
    var headers = {"Content-Type": "text/plain", "Powered-by": "nodejs"};
    response.writeHead(200, headers);
    if (content.name) {
        response.write('bye ' + content.name);
    } else {
        response.write('bye');
    }
    response.end();
}

exports.hello = hello;
exports.bye = bye;