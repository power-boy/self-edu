/**
 * Created by seryogin on 10.02.15.
 */

function hello(response) {
    console.log("Request handler 'Hello' was called.");
    var headers = {"Content-Type": "text/plain", "Powered-by": "nodejs"};
    response.writeHead(200, headers);
    response.write('hello');
    response.end();
}

function bye(response) {
    var headers = {"Content-Type": "text/plain", "Powered-by": "nodejs"};
    response.writeHead(200, headers);
    response.write('bye');
    response.end();
}

exports.hello = hello;
exports.bye = bye;