/**
 * Created by seryogin on 10.02.15.
 */

function route(handle, pathname, response) {
    console.log("About to route a request for " + pathname);
    if (typeof handle[pathname] === 'function') {
        return handle[pathname](response);
    } else {
        console.log("No request handler found for " + pathname);
        var headers = {"Content-Type": "text/plain", "Powered-by": "nodejs"};
        response.writeHead(404, headers);
        response.write('404 Not found');
        response.end();
    }
}

exports.route = route;